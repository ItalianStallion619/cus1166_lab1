from mymodules.models import Student

def __main__():
    roster = []
    roster.append(Student("Andrew", 100))
    roster.append(Student("Tom", 90))
    roster.append(Student("Matt", 95))
    roster.append(Student("Mitt", 80))
    roster.append(Student("Isaac", 95))
    roster.append(Student("Brandon", 90))
    roster.append(Student("Richard", 85))
    roster.append(Student("Timmy", 90))
    roster.append(Student("Mark", 80))
    roster.append(Student("John", 80))
    
    for student in roster:
        student.print_student_info()
__main__()